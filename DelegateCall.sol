// SPDX-License-Identifier: MIT
pragma solidity ^0.8.17;

contract ContractOne {
    uint256 public value;
    address public lastCaller;

    function inc() public {
        value++;
        lastCaller = msg.sender;
    }

    receive() external payable {}

    fallback() external payable {
        lastCaller = msg.sender;
    }
}

contract Caller {
    uint256 public value;
    address public lastCaller;

    function delegateCallInc(address contractAddress) public {
        (bool call, ) = contractAddress.delegatecall(
            abi.encodeWithSignature("incs()")
        );
        require(call, "delegatecall failed.");
    }
}
