// SPDX-License-Identifier: MIT
pragma solidity ^0.8.17;

library FloatLibrary {
    function getPercentage(
        uint256 outOf,
        uint256 percentage,
        uint256 percentageDecimal,
        uint256 precision
    ) public pure returns (uint256 result) {
        uint256 totalAmt = outOf * (10**precision);
        result = (totalAmt * percentage) / (10**(percentageDecimal + 2));
    }
}

contract Floats {
    function deposit() public payable {}

    function amountToSend(uint256 per, uint256 decimal)
        public
        pure
        returns (uint256 amt)
    {
        amt = FloatLibrary.getPercentage(
            100, // it will send out of current balance, if want fix percentage we can define it in global variable
            per,
            decimal,
            0 // coz eth is already interpreted in wei which means 10 ^ 18
        );
    }

    function test() public pure returns (uint) {
        return 69;
    }
}

contract TestDele {
    function hey(address t) public returns (uint256 result) {
        (, bytes memory data) = t.delegatecall(
            abi.encodeWithSignature("test()")
        );

        assembly {
            result := mload(add(data, 0x20))
        }
    }
}
