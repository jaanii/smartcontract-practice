// SPDX-License-Identifier: MIT
pragma solidity ^0.8.17;

contract Lib {
    uint256 public someNumber;

    function pwn(uint256 _value) public {
        someNumber = _value;
    }
}

contract HackMe {
    address public lib;
    address public owner;
    uint256 public someNumber; // move this to the location 1 as it is the first state variable in Lib contract

    constructor(address _lib) {
        owner = msg.sender;
        lib = _lib;
    }

    function pwn(uint256 _value) public {
        (bool done, ) = lib.delegatecall(
            abi.encodeWithSignature("pwn(uint256)", _value)
        );
        require(done, "delegatecall failed");
    }

    function getAddrUint(address a) public pure returns (uint256) {
        return uint256(uint160(a));
    }
}

// contract Attack {
//     address public lib;
//     address public owner;
//     uint256 public someNumber; // move this to the location 1 as it is the first state variable in Lib contract

//     constructor(address _lib) {
//         owner = msg.sender;
//         lib = _lib;
//     }

//     function pwn(uint256 _value) public {
//         (bool done, ) = lib.delegatecall(
//             abi.encodeWithSignature("pwn(uint256)", _value)
//         );
//         require(done, "delegatecall failed");
//     }
// }
