// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

contract Vault {
  bool public locked;
  bytes32 private password;

  constructor(bytes32 _password) {
    locked = true;
    password = _password;
  }

  function unlock(bytes32 _password) public {
    if (password == _password) {
      locked = false;
    }
  }
}

// 0x5E2FB982fe21b6C4555C7D62eFF13FE1BEcfbFf6 -> contract address
// await web3.eth.getStorageAt(contract.address, 1); -> reading 1st slot of the contract storage which is the password
// '0x412076657279207374726f6e67207365637265742070617373776f7264203a29' -> received the password