// SPDX-License-Identifier: MIT
pragma solidity ^0.8.17;
contract MinimalProxy {
    // BYTECODE BROKEN DOWN //

    // Creation Code
    // 3d602d80600a3d3981f3 -> Copy Runtime code into memory

    // Runtime Code
    // 363d3d373d3d3d363d73 -> Code to Delegatecall (part 1)
    // bebebebebebebebebebebebebebebebebebebebe -> we have to replace this address manually (at which we are performing delegate call)
    // 5af43d82803e903d91602b57fd5bf3 -> Code to Delegatecall (part 2)

    function createClone(address target) public returns (address result) {
        bytes20 targetBytes = bytes20(target);

        assembly {
            let clone := mload(0x40) // free memory pointer (read more)

            // storing the first part of the bytecode at the free memory slot takes up 20 bytes
            mstore(clone, 0x3d602d80600a3d3981f3363d3d373d3d3d363d73)

            // moving 20 bytes and storing the target bytes address there (0x14 == 20 in hexadecimal)
            mstore(add(clone, 0x14), targetBytes)

            // moving 20 bytes more to add the rest of the delegate call code of our minimal proxy
            mstore(add(clone, 0x28), 0x5af43d82803e903d91602b57fd5bf3)

            // deploying contract using create(eth to send, start location, size of the code which is 55 bytes in our case)
            result := create(0, clone, 0x37)
        }
    }
}
