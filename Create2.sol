// SPDX-License-Identifier: MIT
pragma solidity ^0.8.9;

contract Factory {
    function getByteCode(address _owner, uint256 _foo)
        public
        pure
        returns (bytes memory)
    {
        bytes memory cre = type(TestContract).creationCode;
        return abi.encodePacked(cre, abi.encode(_owner, _foo));
    }

    function getAddress(bytes memory bytecode, uint256 salt)
        public
        view
        returns (address)
    {
        bytes32 hash = keccak256(
            abi.encodePacked(
                bytes1(0xff),
                address(this),
                salt,
                keccak256(bytecode)
            )
        );

        return address(uint160(uint256(hash)));
    }

    function deploy(bytes memory bytecode, uint256 _salt)
        public
        payable
        returns (address)
    {
        address addr;

        assembly {
            addr := create2(
                callvalue(),
                add(bytecode, 0x20),
                mload(bytecode),
                _salt
            )

            if iszero(extcodesize(addr)) {
                revert(0, 0)
            }
        }

        return addr;
    }
}

contract TestContract {
    address public owner;
    uint256 public foo;

    constructor(address _owner, uint256 _foo) payable {
        owner = _owner;
        foo = _foo;
    }

    function getBalance() public view returns (uint256) {
        return address(this).balance;
    }
}
